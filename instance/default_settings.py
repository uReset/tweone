#!/usr/bin/env python
#coding:utf8

"""This file must be keeped,dont rename or delete this file
and also not need to change the content of this file, you 
can set your config at "../instance/config.py" file """

## 打开或关闭调试模式，生产环境应关闭调式模式
DEBUG     = False

## Enable Test mode
TESTING   = False

## Session 和 Key设置
# 密钥，应当足够强壮
SECRET_KEY = 'TweOne.com'

# 会话 cookie 的名称
# SESSION_COOKIE_NAME = ''

# 会话 cookie 的域。如果没有配置，那么 SERVER_NAME 的所有子域都可以使用 这个 cookie
# SESSION_COOKIE_DOMAIN = ''

# 会话 cookie 的路径。如果没有配置，那么 所有 APPLICATION_ROOT 都可以使用 cookie 。
# 如果没有设置 APPLICATION_ROOT ，那么 '/' 可以 使用 cookie 。
# SESSION_COOKIE_PATH

# 设置 cookie 的 httponly 标志，缺省为 True 。
# SESSION_COOKIE_HTTPONLY = True

# 设置 cookie 的安全标志，缺省为 False 。
# SESSION_COOKIE_SECURE = False

# 常驻会话的存活期，其值是一个 datetime.timedelta 对象。 自 Flask 0.8 开始，其值可以
# 是一个整数， 表示秒数。
# PERMANENT_SESSION_LIFETIME = 

# 日志记录器的名称
# LOGGER_NAME = 

# 服务器的名称和端口号，用于支持子域（如： 'myapp.dev:5000' ）。
# 注意设置为 “ localhost ”没有用，因为 localhost 不 支持子域。
# SERVER_NAME

## 是否激活Blurprint模块,默认为非激活(False),若要激活请设置为 True
# Enable the FaceBook blueprint
FACEBOOK  = False

# Enable the SinaWeiBo blueprint
SINAWEIBO = False

# Enable the Twitter blueprint
TWITTER   = False

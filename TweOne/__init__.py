#!/usr/bin/env python
#coding:utf8


import os,sys
from flask import Flask,render_template



## Init Flask app
app = Flask(__name__,instance_relative_config=True)


# ## Check is this having a config file,requires at least one file from 'config.py' or 'default_config.py'
# ## And loading the configs from config.py or default_config.py
# if not (app.config.from_pyfile('config.py', silent=True) or \
#     app.config.from_pyfile('default_config.py',silent=True)):
#     print 'oops'
#     sys.exit()

try:
    app.config.from_pyfile('default_settings.py')
except IOError,e:
    print e
    print u'\nTips:\nYou must keep the "default_settings.py" file, don\'t delete or rename this file'
    sys.exit()
app.config.from_pyfile('config.py',silent=True)

## Loading the error handlers 
## and callbacks ,teardown callback
from TweOne import error_handlers

from TweOne import tweone
app.add_url_rule('/home',view_func=tweone.index)
app.add_url_rule('/index',view_func=tweone.index)
app.add_url_rule('/auth-app',view_func=tweone.auth_app)


## import and regist backend blueprint
from backend import backend
app.register_blueprint(backend)



## import and register blurprint
if app.config['FACEBOOK']:
    from .FaceBook import facebook
    app.register_blueprint(facebook)
if app.config['SINAWEIBO']:
    from .SinaWeiBo import weibo
    app.register_blueprint(weibo)
if app.config['TWITTER']:
    from .Twitter import twitter
    app.register_blueprint(twitter)
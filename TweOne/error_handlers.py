#!/usr/bin/env python
#coding:utf8


from flask import render_template
from TweOne import app


## Error Handlers
@app.errorhandler(404)
def page_not_found(error):
    return render_template('40x.html'),404


## Callbacks
@app.before_request
def init_db():
    pass

## Teardown Callbacks
@app.teardown_request
def teardown_request(exception):
    pass

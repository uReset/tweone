#!/usr/bin/env python
#coding:utf8


from flask import Blueprint

backend = Blueprint('backend',__name__.split('.')[0],url_prefix='/backend')

def index():
    return 'this is backend.index'

backend.add_url_rule('/',view_func=index)
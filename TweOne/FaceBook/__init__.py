#!/usr/bin/env python
#coding:utf8


from flask import Blueprint


facebook = Blueprint('facebook',__name__.split('.')[0],url_prefix='/facebook')


@facebook.route('/')
def index():
    return 'facebook Blueprint'
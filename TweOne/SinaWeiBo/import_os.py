import os
 
debug = False
if os.uname()[0] == 'Darwin':
    debug = True
    import gntp.notifier
    
if debug:
    gntp.notifier.mini('Hello World')
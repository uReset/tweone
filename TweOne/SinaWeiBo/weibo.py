#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask      import Blueprint,redirect,url_for,request,render_template,jsonify
from api_client import api_client

weibo = Blueprint('weibo',__name__,url_prefix='/weibo')


def index():
    return 'This is weibo blueprint'


def auth_app():
    return redirect(api_client.get_authorize_url())


def callback():
    code = request.args.get('code')
    # api_client = APIClapi_clientient(app_key=APP_KEY, app_secret=APP_SECRET, redirect_uri=CALLBACK_URL)
    # r = api_client.request_access_token(code)
    access_token = '2.00RZVVQBGfuanBf643d99605Krd2eC'
    expires_in = 1383898702.44954 
    api_client.set_access_token(access_token, expires_in)
    return 'Callback Yes <br> Code:%s' % code

def get_status():
    resp = api_client.statuses.user_timeline.get()
    return resp.statuses[0].text

def get_cookie():
    cookie = request.cookies
    return jsonify(cookie)




weibo.add_url_rule('/',view_func=index)
weibo.add_url_rule('/auth-app',view_func=auth_app)
weibo.add_url_rule('/callback',view_func=callback)
weibo.add_url_rule('/get-status',view_func=get_status)
weibo.add_url_rule('/get-cookie',view_func=get_cookie)